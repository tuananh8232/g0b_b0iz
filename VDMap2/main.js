mapboxgl.accessToken = 'pk.eyJ1IjoiaHV5bmd1eWVuMTEyMiIsImEiOiJja25xd291bzQwNDc3Mm9ybzJ2ZHlwZHF6In0.utjdv0dFHcrsBVsQlftQew';

navigator.geolocation.getCurrentPosition(successLocation, errorLocation, {
  enableHighAccuracy: true
})

function successLocation(position) {
  console.log(position.coords);
  setupMap([position.coords.longitude, position.coords.latitude]);
}

function errorLocation() {}

function setupMap(center) {
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v10',
    center: [106.3413887, 16.3045608], // [longitude, latitude]
    zoom: 5.8,
    zoomControl: true
  });

  var featuresArray=[];
  // add locations 
  var arrayCoords=JSON.parse(localStorage.getItem("coordsArray"));
  for (var i=0;i<arrayCoords.length;i++) {
    var obj={
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [arrayCoords[i].coords.lng, arrayCoords[i].coords.lat]
      },
      properties: {
        title: 'Mapbox',
        description: arrayCoords[i].addressName
      }
    }

    featuresArray.push(obj);
  }

  // add current location
  featuresArray.push(
    {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: center
      },
      properties: {
        title: 'Mapbox Current Location',
        description: 'Đại học CNTT (UIT), Đại học Quốc gia TPHCM, Thành phố Thủ Đức, Tp Hồ Chí Minh, Việt Nam'
      }
    }
  )
  
  var geojson = {
    type: 'FeatureCollection',
    features: featuresArray
    // features: [
    // {
    //   type: 'Feature',
    //   geometry: {
    //     type: 'Point',
    //     coordinates: [106.722041010909, 10.793951889656629]
    //   },
    //   properties: {
    //     title: 'Mapbox',
    //     description: 'Landmark 1 - Vinhomes Central Park'
    //   }
    // },
    // {
    //   type: 'Feature',
    //   geometry: {
    //     type: 'Point',
    //     coordinates: [106.78151931326204, 10.883703002163816]
    //   },
    //   properties: {
    //     title: 'Mapbox',
    //     description: 'Ký túc xá Khu B Đại học Quốc gia TP.HCM'
    //   }
    // },
    // {
    //   type: 'Feature',
    //   geometry: {
    //     type: 'Point',
    //     coordinates: [105.79844968867772, 21.012498848347082]
    //   },
    //   properties: {
    //     title: 'Mapbox',
    //     description: 'Trung Hoà, Cầu Giấy, Hà Nội'
    //   }
    // },
    // {
    //   type: 'Feature',
    //   geometry: {
    //     type: 'Point',
    //     coordinates: [109.0269077, 13.9097704]
    //   },
    //   properties: {
    //     title: 'Mapbox',
    //     description: 'Mỹ Thạnh, An Nhơn, Việt Nam'
    //   }
    // },
    // {
    //   type: 'Feature',
    //   geometry: {
    //     type: 'Point',
    //     coordinates: center
    //   },
    //   properties: {
    //     title: 'Mapbox Current Location',
    //     description: 'Đại học CNTT (UIT), Đại học Quốc gia TPHCM, Thành phố Thủ Đức, Tp Hồ Chí Minh, Việt Nam'
    //   }
    // },
    // ]
  };
  
  // add markers to map
  geojson.features.forEach(function(marker) {
  
    // create a HTML element for each feature
    var el = document.createElement('div');
    
    if (marker.properties.title==="Mapbox") {
      if (marker.properties.description=="Trung Hoà, Cầu Giấy, Hà Nội") 
      {
        el.className = 'marker';
      }
      else 
      {
        el.className = 'marker__less';
      }
      var htmls=`
        <div style="text-align: left; font-size: 15px; padding: 5px">
          <div style="margin-bottom: 5px;">
            <label for='' style="font-weight: bold; margin-right: 28px;">Tên</label>
            <span>BN1725</span>
          </div>
          <div style="margin-bottom: 5px;">
              <label for="" style="font-weight: bold; margin-right: 5px;">Địa chỉ</label>
              <span>${marker.properties.description}</span>
          </div>
          <div>
              <label for="" style="font-weight: bold;">Ghi chú</label>
              <p></p>
          </div>
        </div>
      `;
    }
    else {
      el.className = 'marker__pointCurrentLocation';
      var htmls=`
        <div style="text-align: left; font-size: 1.3rem;">
          <div style="margin-bottom: 5px;">
              <label for="" style="font-weight: bold; margin-right: 5px;">Địa chỉ</label>
              <span>${marker.properties.description}</span>
          </div>
          <div>
              <label for="" style="font-weight: bold;">Ghi chú</label>
              <p></p>
          </div>
        </div>
      `;
    }
    
    // make a marker for each feature and add to the map
    new mapboxgl.Marker(el)
      .setLngLat(marker.geometry.coordinates)
      .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
      .setHTML(htmls))
      .addTo(map)
    ;

    el.addEventListener('click', function() {
      var lng=marker.geometry.coordinates[0];
      var lat=marker.geometry.coordinates[1];

      // flyto marker
      map.flyTo({
        center: [lng, lat],
        zoom: 12,
        speed: 0.9,
        essential: true
      })
    }); 
  });  

  // add navigation control
  var nav = new mapboxgl.NavigationControl();
  map.addControl(nav);
}




